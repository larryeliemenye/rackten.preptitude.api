# README #

To set this up you need Node(v0.10.25), Npm, and Mongodb already installed

### What is this repository for? ###

This is the official repo of Preptitude REST API

### How do I get set up? ###

TO set this Project up and start contributing, we are following the git flow convention.
We have the master branch which represents the mainline and the dev branch.
Leave the Master and Main Dev Branch alone as commits will trigger and unit tests on dev and a release build on master. 
To contribute, work directly off a development branch called dev by creating your own dev branch 
called {username}/dev optionally, to keep things even cleaner when working on a specific feature, you
could branch off branch off your local dev into a {feature_name} branch. 

When you are done locally, push to your changes to your local {username}/dev and 
then SEND A PULL REQUEST to the main dev branch. On this branch we run unit tests 
and if everything passes, we move code to master which will trigger a release on Openshift

### Opensift Repos

# Dev
=======
```
git clone ssh://55b787814382ecd266000322@dev-preptitude.rhcloud.com/~/git/dev.git/
```