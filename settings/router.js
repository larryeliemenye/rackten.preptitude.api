/**
 * Created by larry Eliemenye
 */
var exroute = require('exroute');
var routes = require('../routes/index');
var users = require('../routes/users');

module.exports = function (app) {

    /*exroute.init(app, {
        caseSensitive:false,
        mergeParams:false,
        strict:false,
        routesDirectory:'routes/!**!/!*.js'
    });*/

    app.use('/', routes);
    app.use('/users', users)



};