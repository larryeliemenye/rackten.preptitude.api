/**
 * Created by larry Eliemenye
 */
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var mongoose = require(__('lib/database'));

module.exports = function(app){
    app.use(session({
        secret: "somelongassname",
        store: new MongoStore({mongooseConnection:mongoose.connection}),
        resave: true,
        saveUninitialized: true
    }));
};