/**
 * Created by larry Eliemenye
 */

var winston = require('winston');
var expressWinston = require('express-winston');

module.exports = function (app) {
    app.use(expressWinston.errorLogger({
        transports: [
            new winston.transports.Console({
                json: true,
                colorize: true
            }),
            new winston.transports.File({
                filename: './logs/error.log',
                json: false
            })
        ]
    }));

    app.use(expressWinston.logger({
        transports: [
            new winston.transports.Console({
                json: false,
                colorize: true
            }),
            new winston.transports.File({
                filename: './logs/request.log',
                json: false
            })
        ],
        meta: false,
        msg: "HTTP Request {{req.statusCode}} {{req.method}} {{req.url}}",
        colorStatus: true,
        ignoreRoute: function (req, res) {
            return false;
        }
    }));

};