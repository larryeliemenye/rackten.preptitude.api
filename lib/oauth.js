/**
 * Created by larry Eliemenye
 */

var oauth2orize = require('oauth2orize');
var clientModel = require(__('models/Client'));

var clientCredentialsGrant = function clientCredGrantFn(client, scope, done){

};

var clientSerializer = function clientSerializerFn(client, done){
    return done(null,client.id);
};

var clientDeserializer = function clientDeserializerFn(id, done){
    clientModel.findOne({clientId:id}, function(err){

    })
};

var initFn = function initFn(oauthServer){

    //serialization and deserialization of clients
    oauthServer.serializeClient(clientSerializer);
    oauthServer.deserializeClient(clientDeserializer);

    //Supported grant types
    oauthServer.exchange(
        oauth2orize.exchange.clientCredentials(clientCredentialsGrant)
    )
};

exports.init = initFn;
exports.authenticateClient = [
    //TODO:a function to check if user is logged in before providing an allow/deny dialog depending on the grant type we decide to use
];