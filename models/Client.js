/**
 * Created by larry Eliemenye
 */

var mongoose = require(__('lib/database'));
var ObjectId = mongoose.Schema.ObjectId;
var timestamps = require('mongoose-concrete-timestamps');

/**
 *
 * Schema Definition
 *
 * */

var ClientSchema = new mongoose.Schema(
    {
        name: {type: String, unique: true},
        id: {type: String, unique: true},
        secret: {type: String},
        userId: {type: ObjectId}
    }
);

/**
 *
 * One ore more plugin to add to shcmea
 *
 * */

ClientSchema.plugin(timestamps);

/**
 *
 * Exports only an instance of the schema's model
 *
 * */

module.exports = mongoose.model('Client', ClientSchema);