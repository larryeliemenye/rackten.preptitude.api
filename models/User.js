/**
 * Created by larry Eliemenye
 */

var mongoose = require(__('lib/database'));
var timestamps = require('mongoose-concrete-timestamps');
var autoInc = require('mongoose-auto-increment');

/**
 *
 * Schema Definition
 *
 * */

var UserSchema = new mongoose.Schema(
    {
        firstname: {type: String, unique: true},
        lastname: {type: String, unique: true},
        organization: {type: String},
        userId:Number,
        hash: {type: String},
        email: {type: String}
    }
);

/**
 *
 * One ore more plugin to add to shcmea
 *
 * */

UserSchema.plugin(timestamps);
UserSchema.plugin(autoInc.plugin, { model:'User', field:'userId', startAt:1000 });

/**
 *
 * when password is requested, return hash
 *
 * */
UserSchema.virtual("password").get(function () {
    return this.hash;
});

/**
 *
 * When password is set, hash it.
 *
 * */

UserSchema.virtual("password").set(function (password) {
    var passwordhash = crypto
        .createHash("sha1")
        .update(password)
        .digest('hex');
    this.hash = passwordhash;
});

/**
 *
 * Exports only an instance of the schema's model
 *
 * */

module.exports = mongoose.model('User', UserSchema);